import React from 'react';
import './PokemonCard.css'
import { Row, Col } from 'reactstrap';
import pokemonIcon from "../images/pokemonIcon.png";

const PokemonCard = ({label, imageUrl, data}) => (
  <Col sm="12" md="6" lg="4" xl="3" className="pokemon-card-container">
    <div className="pokemon-card">
      <div className="pokemon-card-image-title">
        <img className="pokemon-card-image" alt={label} src={ imageUrl }></img>
        <h4>{label}</h4>
      </div>
      <div className="pokemon-card-abilities-container">
        <h5 className="pokemon-card-abilities-container-heading">Abilities</h5>
        {
          data.abilities.map((ability)=>{
            return (
            <div key={`${ability.ability.name} ${ability.ability.slot} ${ability.ability.is_hidden}`}>
              <img alt="pokemon icon" className="pokemon-card-image-pokemonIcon" src={pokemonIcon}></img>
              <span>{ability.ability.name}</span>
            </div>)
          })
        }
      </div>
      <div className="pokemon-card-stats-container">
        <div className="pokemon-card-stats-heading-container">
          <h5 className="pokemon-card-stats-heading">Stats</h5>
        </div>
          <Row>
            <Col style={{ textAlign: 'center'}}>
              <span>
                Weight: {data.weight}
              </span>
            </Col>
            <Col style={{ textAlign: 'center'}}>
              <span>
                Height: {data.height}
              </span>
            </Col>
          </Row>
      </div>
    </div>
  </Col>
);

export default PokemonCard;