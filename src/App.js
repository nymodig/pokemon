import React, { Component } from 'react';
import './App.css';
import Button from './Button/Button';
import PokemonCard from './PokemonCard/PokemonCard';
import { Container, Row } from 'reactstrap';
import logo from './images/logo.png'
class App extends Component {
  state = { pokemons: [], offSet: 20, loaded: false }

  componentDidMount() {
    this.fetchPokemons();
  }

  checkOffset = (onClick) => {
    if (!onClick) return this.state.offSet;
    if (onClick === 'back' && this.state.offSet === 20) return this.state.offSet;
    if (onClick === 'back' ) {
      const offSet = this.state.offSet - 20;
      this.setState({ offSet })
      return offSet;
    } 
    if (onClick === 'forward' ){
      const offSet = this.state.offSet + 20;
      this.setState({ offSet })
      return offSet;
    } 
  }


  fetchPokemons = async (onClick) => {
    const getPokemons = await fetch(`https://pokeapi.co/api/v2/pokemon/?offset=${this.checkOffset(onClick)}&limit=20`).then((res)=>{
      return res.json();
    })
    const promises = getPokemons.results.map( async (pokemon, index)=> {
      const getPokemon = await fetch(pokemon.url).then((res)=> {
        return res.json();
      }).then((poke)=>{
        const obj = {
          image: poke.sprites.front_default,
          data: {
            abilities: poke.abilities,
            weight: poke.weight,
            height: poke.height,
          }
        }
        return obj;
      })
      const obj = {
        index,
        name: pokemon.name,
        image: getPokemon.image,
        data: getPokemon.data,
      }
      return obj;
    })
    const payload = await Promise.all(promises);
    this.setState({pokemons: payload, loaded: true})
  }

  render() {
    const { pokemons } = this.state;
    return (
      <main>
        <div className="app-logo-container">
          <img src={logo} className="app-logo"></img>

        </div>
        <header className="app-header">
          <Button label="back" onClick={()=> this.fetchPokemons('back')}/>
          <Button label="forward" onClick={()=> this.fetchPokemons('forward')}/>
        </header>
        <Container fluid>
          <Row>
            {
              pokemons.map((pokemon)=>{
                return <PokemonCard key={pokemon.name} data={pokemon.data} imageUrl={pokemon.image} label={pokemon.name} />
              })
            }
          </Row>
        </Container>
        {
          this.state.loaded &&
            <footer className="app-footer">
              <Button label="back" onClick={()=> this.fetchPokemons('back')}/>
              <Button label="forward" onClick={()=> this.fetchPokemons('forward')}/>
            </footer>
        }
      </main>
    );
  }
}

export default App;
